
require 'sqlite3'
require 'json'

module Trees

  @db = SQLite3::Database.new ".elk.db"

  rows = @db.execute <<-SQL
  create table if not exists trees (
    title text not null,
    body text not null,
    parent_id int,
    line_number int
  );
  SQL

  def self.seed()
    if not self.get(1)
      @db.execute "
      insert into trees (title, body) values (?, ?)
      ", ["My first tree", "What a wonderful world this is."]
    end
  end

  def self.get(id)
    res = convert @db.execute("select * from trees where rowid=?", [id])[0]
    if res
      res['rowid'] = id
      link_mapping = self.get_links(id).map {|l| [l['line_number'], l]}.to_h
      res['initial_links'] = JSON.dump(link_mapping)
      res
    else
      nil
    end
  end

  def self.update_title(id, title)
    @db.execute("update trees set title=? where rowid=?", [title, id])
  end

  def self.update_body(id, body)
    @db.execute("update trees set body=? where rowid=?", [body, id])
  end

  def self.get_links(parent)
    @db.execute(
      "select title, line_number, parent_id, rowid from trees where parent_id=?",
      [parent]
    ).map { |x| ['title', 'line_number', 'parent_id', 'rowid'].zip(x).to_h }
  end

  def self.create_child(parent_id:, line_number:)
    @db.execute("
    insert into trees (title, body, parent_id, line_number) values (?, ?, ?, ?)
    ", ["[Great Title]", "[Great Content]\n", parent_id, line_number])
  end

  private

    @fields = ['title', 'body', 'parent_id', 'line_number']

    def self.convert(data)
      @fields.zip(data).to_h if not data.nil?
    end


end

