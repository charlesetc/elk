

function post_to(path, data) {
  const xhttp = new XMLHttpRequest();
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(data);
}

function flash_save(o) {
  o.classList.add('save');
  setTimeout(() => o.classList.remove('save'), 200);
}

function save_title(title) {
  post_to(`/${rowid}/title`, `title=${title.innerHTML}`);
  flash_save(title);
}

function save_body(body) {
  post_to(`/${rowid}/body`, `body=${body.innerHTML}`);
  flash_save(body);
}

function adjust_links(links, body, override) {
  let total_lines = body.children.length;
  if (total_lines == 0) total_lines = 1;

  if (override || links.children.length != total_lines) {
    links.innerHTML = '' // blank slate
    for(let i = 0; i < total_lines; i++) {
      let cls, f;
      if (initial_links[i]) {
        const data = initial_links[i];
        f = () => go_to_note(data['rowid']);
        cls = 'real';
      } else {
        f = () => create_new_note(links, body, i);
        cls = 'fake';
      }
      link = document.createElement("div");
      link.classList.add('link');
      link.classList.add(cls);
      link.onclick = f;
      links.appendChild(link);
    }
  }
}

function create_new_note(links, body, line_number) {
  post_to(`/${rowid}/child/${line_number}`, null);
  adjust_links(links, body, true);
}

function go_to_note(rowid) {
  window.location.href =`/${rowid}`;
}

function global_shortkeys(e) {
  if (e.shiftKey && e.keyCode == 13) {
    // shift-enter pressed

    return false;
  } else if (e.keyCode == 27) {
    // escape pressed
    if (parent_id) {
      go_to_note(parent_id);
      return false;
    }
  }
}

function main() {
  const body = document.getElementById("body");
  const title = document.getElementById("title");
  const links = document.getElementById("links");

  title.onblur = (e) => save_title(title);
  body.onblur = (e) => save_body(body);
  body.oninput = (e) => adjust_links(links, body);
  adjust_links(links, body);
  setTimeout(() => { body.focus(); body.selectionStart = body.selectionEnd = 10000; }, 0);

  document.onkeydown = global_shortkeys;

}

window.onload = main;
