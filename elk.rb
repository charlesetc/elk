
require './trees'
require 'sinatra'

set :public_folder, "static"

Trees.seed

get '/:id' do |id|
  tree = Trees.get(id)
  if tree.nil?
    not_found "Unknown tree"
  else
    erb :tree, {}, tree
  end
end

post '/:id/title' do |id|
  Trees.update_title(id, params[:title])
end

post '/:id/body' do |id|
  Trees.update_body(id, params[:body])
end

post '/:parent_id/child/:ln' do |parent_id, ln|
  Trees.create_child(parent_id: parent_id, line_number: ln)
end
